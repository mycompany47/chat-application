import mongoose from "mongoose";
import bcrypt from "bcryptjs";

const userSchema = mongoose.Schema(
    {
        name: { type: String, required: true },
        email: { type: String, required: true, unique: true },
        password: { type: String, required: true },
        picture: {
            type: String,
            default: "https://icon-library.com/images/anonymous-avatar-icon/anonymous-avatar-icon-25.jpg"
        }
    },
    {
        timeStamps: true
    })

    //this is the function where we match entered password and db password
    userSchema.methods.matchedPassword = async function (enteredPassword) {
        return await bcrypt.compare(enteredPassword, this.password)
    }

    // this code means before saving the password first encrypt or encode it
    userSchema.pre('save', async function (next) {
        if (!this.isModified) {
            next()
        }
        const salt = await bcrypt.genSalt(10);
        console.log("salt-----------------",salt)
        this.password = await bcrypt.hash(this.password, salt)
        console.log("salt----------", salt)
    })

const userModel = mongoose.model("User", userSchema)



export default userModel