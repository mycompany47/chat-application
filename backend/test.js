const nodemailer = require('nodemailer');
const fs = require('fs');

// Create a Nodemailer transporter
const transporter = nodemailer.createTransport({
  service: 'your_email_service_provider',
  auth: {
    user: 'your_email@example.com',
    pass: 'your_email_password'
  }
});

// Read the PDF file
const filePath = '/path/to/your/pdf/file.pdf';
const fileName = 'file.pdf'; // You can adjust the file name as needed

fs.readFile(filePath, (err, data) => {
  if (err) {
    console.error('Error reading file:', err);
    return;
  }

  // Compose email
  const mailOptions = {
    from: 'your_email@example.com',
    to: 'recipient@example.com',
    subject: 'PDF Attachment',
    text: 'Please find the attached PDF file.',
    attachments: [
      {
        filename: fileName,
        content: data // Attach the file content directly
      }
    ]
  };

  // Send the email
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error('Error sending email:', error);
    } else {
      console.log('Email sent:', info.response);
    }
  });
});
