import express from "express";
import { registerUser, loginUser, allUsers } from "../controllers/userControllers.js";
import  protectRoute  from "../middleware/authmiddleWare.js";

const router = express.Router();

/*
if we want to chain the multiple requests then we use router.route()
For Eg:--router.route('/login').get(()=>{}).post(()=>{});
*/

// endpoint for register
router.route('/').post(registerUser).get(protectRoute, allUsers)

// endpoint for login

router.post('/login', loginUser)



export default router;
