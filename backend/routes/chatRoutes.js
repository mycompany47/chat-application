import express from "express";
import { accessChat, fetchChats, renameGroup, addToGroup, createGroupChat, removeFromGroup } from "../controllers/chatControllers.js";
import  protectRoute  from "../middleware/authmiddleWare.js";


const router = express.Router();
router.route("/").post(protectRoute, accessChat)
router.route("/").get(protectRoute, fetchChats)
router.route("/group").post(protectRoute, createGroupChat)
// --------------- put request because we need to update the database----------
router.route("/rename").put(protectRoute, renameGroup)
router.route("/groupremove").put(protectRoute, removeFromGroup)
router.route("/groupadd").put(protectRoute, addToGroup)


export default router