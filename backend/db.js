
import mongoose from 'mongoose';

const mongoURI = process.env.MONGO_URI;
console.log("mongo-------", mongoURI)

const connectDB = async () => {
  try {
    if (!mongoURI) {
      throw new Error('MONGO_URI is not set. Make sure to set the environment variable.');
    }

    const options = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };

    await mongoose.connect(mongoURI, options);

    console.log('Connected to MongoDB');
  } catch (error) {
    console.error('Error connecting to MongoDB:', error.message);
  }
};

export default connectDB;
