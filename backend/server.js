import 'dotenv/config'
import express from "express";
import connectDb from "./db.js";
import colors from "colors";
import cors from "cors";
import userRoutes from "./routes/userRoutes.js"
import chatRoutes from "./routes/chatRoutes.js";

import {notFound, errorHandler} from "./middleware/errormiddleware.js"
const app = express()


// Enable CORS for all routes
app.use(cors());

connectDb();


const port = process.env.PORT || 5000;
    
app.use(express.json()) // it is used to tell the server to accept json data
console.log("port" , port)
app.use('/api/chat', chatRoutes)
app.use('/api/user', userRoutes)

app.use(notFound)
app.use(errorHandler)

app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`.yellow.bold)
})