import asyncHandler from "express-async-handler";
import Chat from "../models/chatModel.js";
import User from "../models/userModel.js";


//@description     Create or fetch One to One Chat
//@route           POST /api/chat/
//@access          Protected
const accessChat = asyncHandler(async (req, res) => {

    const { userId } = req.body;

    if (!userId) {
        console.log("UserId param not sent with request");
        return res.sendStatus(400);
    }

    /* to find a chat that is not a group chat and
            involves both the current user (req.user._id) 
            and the specified userId. 
            Populates the users field (excluding the password) and the latestMessage field.*/

    let isChat = await Chat.find({
        isGroupChat: false,
        $and: [
            { users: { $elemMatch: { $eq: req.user._id } } },
            { users: { $elemMatch: { $eq: userId } } },
        ],
    })
        .populate("users", "-password")
        .populate("latestMessage");

    // Populates the latestMessage.sender field of the found chat, selecting specific fields.

    isChat = await User.populate(isChat, {
        path: "latestMessage.sender",
        select: "name pic email",
    });


    // Checks if a chat was found. If yes, sends the found chat. If not, prepares data to create a new chat.

    if (isChat.length > 0) {
        res.send(isChat[0]);
    } else {
        let chatData = {
            chatName: "sender",
            isGroupChat: false,
            users: [req.user._id, userId],
        };

        /* Tries to create a new chat with the provided data and sends the newly created chat as JSON response.
            If an error occurs during the process, it sends a 400 status and throws an error.*/
        try {
            const createdChat = await Chat.create(chatData);
            const FullChat = await Chat.findOne({ _id: createdChat._id }).populate(
                "users",
                "-password"
            );
            res.status(200).json(FullChat);
        } catch (error) {
            res.status(400);
            throw new Error(error.message);
        }
    }


});


//@description     Fetch all chats for a user
//@route           GET /api/chat/
//@access          Protected
const fetchChats = asyncHandler(async (req, res) => {
    try {

        /*  Uses Mongoose to find chats where the current user 
            (req.user._id) is one of the users in the chat.
        */
        Chat.find({ users: { $elemMatch: { $eq: req.user._id } } })

            /*Populates the users field of the found chats, 
              excluding the password field.*/
            .populate("users", "-password")

            /*Populates the groupAdmin field of the found chats, excluding the password field.*/
            .populate("groupAdmin", "-password")

            // Populates the latestMessage field of the found chats.
            .populate("latestMessage")

            // Sorts the results based on the updatedAt field in descending order which means(latest first).
            .sort({ updatedAt: -1 })

            /* Executes a callback function when the find operation is completed. 
             It awaits the population of the latestMessage.sender field.*/
            .then(async (results) => {

                // Populates the latestMessage.sender field of the found chats with additional user data (name, pic, email).
                results = await User.populate(results, {
                    path: "latestMessage.sender",
                    select: "name pic email",
                });

                /*Catches any errors that may occur during the process. 
                    If an error occurs, it sends a 400 status and throws an error. 
                    If successful, it sends the populated results as a response with a 200 status.*/
                res.status(200).send(results);
            });
    } catch (error) {
        res.status(400);
        throw new Error(error.message);
    }
});


//@description     Create New Group Chat
//@route           POST /api/chat/group
//@access          Protected
const createGroupChat = asyncHandler(async (req, res) => {
    if (!req.body.users || !req.body.name) {
        return res.status(400).send({ message: "Please Fill all the feilds" });
    }

    // Parses the "users" field from the request body as JSON and assigns it to the variable users
    let users = JSON.parse(req.body.users);
    console.log("users---", users)

    // Checks if the number of users in the parsed array is less than 2.
    if (users.length < 2) {
        return res
            .status(400)
            .send("More than 2 users are required to form a group chat");
    }

    // Adds the current user which is making the request 
    users.push(req.user);


    // Uses the Chat.create method to create a new chat group with specified 
    // properties (name, users, isGroupChat, groupAdmin). It awaits the completion of this asynchronous operation.
    try {
        const groupChat = await Chat.create({
            chatName: req.body.name,
            users: users,
            isGroupChat: true,
            groupAdmin: req.user,
        });

        // Uses the Chat.findOne method to find the newly created chat group by its _id 
        // and populates the "users" and "groupAdmin" fields, excluding the "password" field.

        const fullGroupChat = await Chat.findOne({ _id: groupChat._id })
            .populate("users", "-password")
            .populate("groupAdmin", "-password");

        // Sends a 200 status response with the created and populated group chat object in JSON format.
        res.status(200).json(fullGroupChat);
    } catch (error) {
        res.status(400);
        throw new Error(error.message);
    }
});


// @desc    Rename Group
// @route   PUT /api/chat/rename
// @access  Protected
const renameGroup = asyncHandler(async (req, res) => {
    const { chatId, chatName } = req.body;
  
    const updatedChat = await Chat.findByIdAndUpdate(
      chatId,
      {
        chatName: chatName,
      },
      {
        new: true,
      }
    )
      .populate("users", "-password")
      .populate("groupAdmin", "-password");
  
    if (!updatedChat) {
      res.status(404);
      throw new Error("Chat Not Found");
    } else {
      res.json(updatedChat);
    }
  });
  
  // @desc    Remove user from Group
  // @route   PUT /api/chat/groupremove
  // @access  Protected
  const removeFromGroup = asyncHandler(async (req, res) => {
    const { chatId, userId } = req.body;
  
    // check if the requester is admin
  
    const removed = await Chat.findByIdAndUpdate(
      chatId,
      {
        $pull: { users: userId },
      },
      {
        new: true,
      }
    )
      .populate("users", "-password")
      .populate("groupAdmin", "-password");
  
    if (!removed) {
      res.status(404);
      throw new Error("Chat Not Found");
    } else {
      res.json(removed);
    }
  });
  
  // @desc    Add user to Group / Leave
  // @route   PUT /api/chat/groupadd
  // @access  Protected
  const addToGroup = asyncHandler(async (req, res) => {
    const { chatId, userId } = req.body;
  
    // check if the requester is admin
  
    const added = await Chat.findByIdAndUpdate(
      chatId,
      {
        $push: { users: userId },
      },
      {
        new: true,
      }
    )
      .populate("users", "-password")
      .populate("groupAdmin", "-password");
  
    if (!added) {
      res.status(404);
      throw new Error("Chat Not Found");
    } else {
      res.json(added);
    }
  });


export { accessChat, fetchChats, createGroupChat , renameGroup , removeFromGroup , addToGroup};

