import asyncHandler from "express-async-handler";
import User from "../models/userModel.js";
import generateToken from "../config/generateToken.js";
import formidable from "formidable";
import path from "path";
import fs from "fs";

// to register user
const registerUser = asyncHandler(async (req, res) => {
    console.log("req---------------", req)
    const form = formidable({
        maxFiles: 1,
        filename: () => `${Date.now() + 'image.jpg'}`,
        uploadDir: path.join(process.cwd(), 'uploads/images'),
    });

    form.parse(req, async (err, fields, files) => {

        // const { name, email, password } = fields;

        let picPath = files.pic[0]._writeStream.path;
        console.log("pic-------------", picPath)

        if (err) {
            res.status(400).json({ error: "Image could not be uploaded" });
            return;
        }

        const name = Array.isArray(fields.name) ? fields.name[0] : fields.name;
        const email = Array.isArray(fields.email) ? fields.email[0] : fields.email;
        const password = Array.isArray(fields.password) ? fields.password[0] : fields.password;

        const userExists = await User.findOne({ email });

        if (userExists) {
            res.status(400).json({ error: "User already exists" });
            return;
        }

        // Create user with the image path
        const user = await User.create({
            name,
            email,
            password,
            picture: picPath, // Save the path to the database
        });

        if (user) {
            res.status(201).json({
                _id: user._id,
                name: user.name,
                email: user.email,
                password: user.password,
                picture: user.picture,
                token: generateToken(user._id),
            });
        } else {
            res.status(400).json({ error: "Failed to create the user" });
        }
    });
});


//to login user
const loginUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    console.log("user---", user)
    if (user && (await user.matchedPassword(password))) {
        console.log("uesend lskdbskkdnsc sdcnbosdn", user)
        res.json({
            _id: user._id,
            name: user.name,
            email: user.email,
            pic: user.pic,
            token: generateToken(user._id)
        })
    } else {
        res.status(401);
        throw new Error("Invalid Email or Password")
    }
})

// /api/user?search=piyush
// to render all Users
const allUsers = asyncHandler(async (req, res) => {
    const { name } = req.query;
    console.log("name user---", name)
    if (!name) {
        return res.status(400).json({ error: 'Enter the name to search' });
    }

    try {
        const users = await User.find({ name: { $regex: name, $options: 'i' } });
        res.json(users);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }

});


export { registerUser, loginUser, allUsers };

