import { configureStore } from '@reduxjs/toolkit';
import nameReducer from './nameSlice';
import searchReducers from "./usersSlice"

const store = configureStore({
  reducer: {
    name: nameReducer,
    searchData: searchReducers,
  },
});

export default store;
