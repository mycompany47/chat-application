import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const nameThunk = {
    sendName: createAsyncThunk(
        'sendName', async (data, thunkAPI) => {
            console.log("Data-----", data)
            const response = await axios.post('https://dummyjson.com/posts/add', { title: data.title, userId: data.userId });
            return response.data;
        }
    )
}

const nameSlice = createSlice({
    name: 'name',
    initialState: {
        name: "",
        id: '',
        status: 'idle',
        error: null,
        value: ""
    },


    // static
    reducers: {
        setName(state, action) {
            console.log("State name---", action.payload)
            state.name = action.payload;
        },
        setId(state, action) {
            console.log("state id====", state.id)
            state.id = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(nameThunk.sendName.pending, (state) => {
                console.log("state pen----", state.status)
                state.status = 'loading';
                console.log("state---", state)
            })
            .addCase(nameThunk.sendName.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.response = action.payload;
            })
            .addCase(nameThunk.sendName.rejected, (state, action) => {
                console.log("state pen3----", state.status)
                state.status = 'failed';
                state.error = action.error.message;
            });
    },
});

export const nameAction = {
    setName: nameSlice.actions,
    setId: nameSlice.actions,
}

// export all static actions
export const { setName, setId } = nameSlice.actions;

export default nameSlice.reducer;

