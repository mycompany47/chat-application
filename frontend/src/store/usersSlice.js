import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import api from '@/utils/api';
import routes from '@/utils/routes';
export const allUserThunk = {
    searchUsers: createAsyncThunk(
        'searchUsers', async (search, thunkAPI) => {
            const response = await api.get(routes.USER_GET.GET_SEARCH_USERS, {
                params: {
                    name: search
                }
            });
            return response.data;
        }
    ),

    useraccessChat: createAsyncThunk(
        'useraccessChat', async (getUserChat, thunkAPI) => {
            // GET_USER_CHAT
            let data = {}
            data["userId"] = getUserChat
            const res = await api.post(routes.USER_GET.GET_USER_CHAT, data)
            console.log("responsed-----", res)
            return res.data;
        }
    )
}

const userSlice = createSlice({
    name: 'user',
    initialState: {
        user: "",
        status: '',
        error: null,
        value: "",
        response: "",
        showModal: true,
        useraccessChatResponse: "",
    },


    // static
    reducers: {
        setUser(state, action) {
            console.log("user-----------", action)
            state.user = action.payload;
        },
        closeModal(state, action) {
            state.showModal = action.payload
        },

    },
    extraReducers: (builder) => {
        builder
            .addCase(allUserThunk.searchUsers.pending, (state) => {
                console.log("state pen----", state.status)
                state.status = 'loading';
                console.log("state---", state)
            })
            .addCase(allUserThunk.searchUsers.fulfilled, (state, action) => {
                console.log("dknsk----", action.payload)
                state.response = action.payload
                state.status = 'succeeded';
            })
            .addCase(allUserThunk.searchUsers.rejected, (state, action) => {
                console.log("state pen3----", state.status)
                state.status = 'failed';
                state.error = action.error.message;
            })
            .addCase(allUserThunk.useraccessChat.pending, (state) => {
                console.log("state pen----", state.status)
                state.status = 'loading';
            })
            .addCase(allUserThunk.useraccessChat.fulfilled, (state, action) => {
                console.log("asdasdamn", action)
                state.useraccessChatResponse = action.payload
                state.status = 'succeeded';
            })
            .addCase(allUserThunk.useraccessChat.rejected, (state, action) => {
                console.log("state pen3----", state.status)
                state.status = 'failed';
                state.error = action.error.message;
            });

    },
});

export const userAction = {
    setUser: userSlice.actions,
    closeModal: userSlice.actions,
}

// export all static actions
export const { setUser, closeModal } = userSlice.actions;

export default userSlice.reducer;

