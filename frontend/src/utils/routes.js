
export default {
    USER_GET: {
        GET_SEARCH_USERS : "/api/user",
        GET_USER_CHAT: "/api/chat"
    }
}