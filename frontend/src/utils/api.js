import axios from "axios";

const url = process.env.NEXT_PUBLIC_URL;

let userLogged = ""
console.log("userLogged==", userLogged)

if (typeof window !== 'undefined') {
    const loggedData = localStorage.getItem("isloggedChatApp");
    if (loggedData) {
        userLogged = JSON.parse(loggedData).token;
    }

    // to ensure redirection happens only once
    if (!userLogged && !window.location.href.includes("/user_signup")) {
        window.location.href = "/user_signup";
    }
}



const api = axios.create({
    baseURL: `${url}`,
    headers: {
        "Accept": 'application/json',
        "Content-Type": 'application/json',
        Authorization: userLogged ? `Bearer ${userLogged}` : ''
    }
})

export default api;
