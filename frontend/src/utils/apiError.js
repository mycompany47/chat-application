import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const apiError = (message) => {
   return toast.error(message);
}

export default apiError;
