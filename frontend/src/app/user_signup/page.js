"use client";
import { React, useState, useEffect } from 'react';
import { Container, Button, Col, Tab, Tabs, Form, InputGroup, Row } from "react-bootstrap";
import styles from "../../../src/app/styles/auth.module.css"
import Registration from '../components/Registration';
import chatWindowImage from "../../app/assets/chat.jpg"
import Login from '../components/Login';
import Image from "next/image";
const Home = () => {

  const [login, setLogin] = useState(true)

  // api/user/login



  const handleLogin = () => {
    setLogin(false)
  }

  const handleRegistration = () => {
    setLogin(true)
  }


  return (
    <>
      {/* <Container> */}
        <Row>
          <Col xs={12} md={6}>
            <div className={styles.BoxForAuth}>
              <div className={styles.BoxForTab}>
                <div className={styles.navigate} onClick={handleLogin}>Login</div>
                <div className={styles.navigate} onClick={handleRegistration}>Registration</div>
              </div>

              {login ? <div>
                <Registration></Registration>
              </div> : <div><Login></Login></div>}
            </div>
          </Col>
          <Col xs={6} md={6} className={styles.gridBoxParent}>
            <div className={styles.gridBox}>
              <Image src={chatWindowImage} width={600}  alt="chat image"></Image>
            </div>
          </Col>
        </Row>
      {/* </Container> */}
    </>
  )
}

export default Home
