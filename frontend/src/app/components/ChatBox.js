import React from "react";
import { Container, Row, Col } from 'react-bootstrap';
import styles from "../styles/chat_box.module.css";
import PeopleChatList from "./PeopleChatList";
import Image from "next/image";

import NoChatImage from "../../../src/app/assets/handDraw.jpg"

const ChatBox = () => {
    return (<>
        <div className={styles.chat_box}>
            <Row>
                <Col md={4} sm={12}>
                    <div className={styles.chat_list_box}>
                            <PeopleChatList></PeopleChatList>
                    </div>
                </Col>
                <Col md={8} sm={12}>
                    <div className={styles.chat_list_box}>
                        {/* this div render when there is no chat */}
                        <div className={styles.box_not_found_flex}>
                            <Image src={NoChatImage} className={styles.image_not_found} alt="chat image" ></Image>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>

        <div>
        </div>

        
    </>)
}


export default ChatBox;