import React, { useState } from "react";
import { getSelectedUsers } from "@/store/usersSlice";
import { useDispatch, useSelector } from "react-redux";
import styles from "../styles/chat_box.module.css";
import { ListGroup, Placeholder, Card, Button ,Modal, Form } from 'react-bootstrap';
import { ArrowRight } from 'react-bootstrap-icons';



const PeopleChatList = () => {

    const [show, setShow] = useState(false);

    const useraccessChatResponse = useSelector((state) => {
        console.log("skjdnskdjnfsknn", state.searchData)
        return state.searchData.useraccessChatResponse.users
    });

    console.log("useraccessChatResponse=====", useraccessChatResponse)

    const handleClose = () => setShow(false);

    const handleGroupChat = () => {
        setShow(true)
    }

    const handleSelectChat = (index) => {
        console.log("index----", index)
       const filteredData = useraccessChatResponse.filter((element) => {
            console.log("element-----", element._id)
            return element._id === index
        })

        console.log("filteredData=====", filteredData)
    }


    return (<>

        <div className={styles.BoxForChatList}>
            <div className={styles.chatHead}>
                <div className={styles.ChatHeading}>
                    <h3>My Chats</h3>
                </div>
                <div>
                    <Button variant="primary" className={styles.GroupBtnCreate} onClick={handleGroupChat}>New Group Chat <ArrowRight style={{ marginLeft: "5px" }} /></Button>
                </div>
            </div>


            {useraccessChatResponse ?
                <div>
                    {useraccessChatResponse.map((element) => {
                        console.log("element----", element)
                        return (<>
                            <div key={element._id} className={styles.ListGroupDiv} onClick={(e) => handleSelectChat(element._id)}>
                                <div className={styles.ImageFrame}>
                                    {/* <img src={element.picture} className={styles.ImageCircular}></img> */}
                                </div>
                                <div className={styles.name_email}>
                                    <p>{element.name}</p>
                                    <p className={styles.email}>{element.email}</p>
                                </div>
                            </div>

                        </>)
                    })

                    }
                </div>
                :
                null
            }
        </div>


        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Create Group Chat</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="name@example.com"
                            autoFocus
                        />
                    </Form.Group>
                    <Form.Group
                        className="mb-3"
                        controlId="exampleForm.ControlTextarea1"
                    >
                        <Form.Label>Example textarea</Form.Label>
                        <Form.Control as="textarea" rows={3} />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleClose}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>

    </>)
}

export default PeopleChatList