"use client";
import React, { useState } from 'react';
import { Container, Button, Col, Form, InputGroup, Row } from "react-bootstrap";
import Image from 'react-bootstrap/Image';
import styles from "../styles/auth.module.css"; // Updated import
import axios from 'axios';

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = async () => {
      try {
        const res = await axios.post("http://localhost:3000/api/user/")
      } catch (error) {
        
      }
  };

  const GuestLogin = () => {
    // Add guest login functionality
  };

  return (
    <>
      <Container>
        <div className={styles.FormBox}>
          <div className={styles.Form}>
            <div className={styles.Logo}>
              {/* <Image  alt="Image Alt Text" fluid /> */}
            </div>
            <div className={styles.Heading}>
              <h5>Login Here</h5>
            </div>
            <Form>
              <Row className="mb-3">
                <Form.Group as={Col} md="12" controlId="validationCustomUsername">
                  <Form.Label><b>Email</b></Form.Label>
                  <InputGroup hasValidation>
                    <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                    <Form.Control
                      type="text"
                      placeholder="Username"
                      aria-describedby="inputGroupPrepend"
                      required
                      className={styles.inputBox}
                      onChange={e => {
                        setEmail(e.target.value);
                        console.log(e.target.value);
                      }}
                    />
                  </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="12" className={styles.FormFeild} controlId="validationCustom02">
                  <Form.Label><b>Password</b></Form.Label>
                  <Form.Control
                    required
                    type="password"
                    placeholder="Password"
                    defaultValue="Otto"
                    className={styles.inputBox}
                    onChange={e => setPassword(e.target.value)}
                  />
                </Form.Group>
              </Row>
              <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                <div className={styles.BtnBox}>
                  <Button type="button" onClick={handleLogin} className={styles.btnAuth}>Login</Button>
                </div>
                <div className={styles.BtnBox}>
                  <Button type="button" onClick={GuestLogin} className={styles.btnAuth}>Guest Credential</Button>
                </div>
              </div>
            </Form>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Login;
