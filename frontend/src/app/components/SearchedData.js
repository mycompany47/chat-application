import React, { useState, useEffect } from "react";
import styles from "../styles/chat.module.css";
import { useDispatch, useSelector } from 'react-redux';
import apiError from "../../utils/apiError";
import { Row, Col, Form, ListGroup, Badge, Dropdown, Placeholder, Card } from 'react-bootstrap';
import LoadingSkeleton from "./LoadingSkeleton";
import { allUserThunk, closeModal, getSelectedUsers } from "@/store/usersSlice";

const SearchedData = () => {

    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)
    // grabing status
    const status = useSelector((state) => state.searchData.status)
    // grabing data
    const dataResponse = useSelector((state) => state.searchData.response)

    

    useEffect(() => {
        switch (status) {
            case "loading":
                setLoading(true)
                break;
            case "succeeded":
                setLoading(false)
                break;
            case "failed":
                apiError("Enter the name to Search")
                break;
            default:
                break;
        }
    }, [status])

    const accessChat = (userId) => {
        dispatch(allUserThunk.useraccessChat(userId))
        dispatch(closeModal(false))

    }


    return (<>

        <div className={styles.data_users}>
            {
                !loading ?

                    <ListGroup as="ol">

                        {dataResponse.length != 0 ? dataResponse.map((element) => {
                            return (<>
                                <ListGroup.Item
                                    onClick={e => accessChat(element._id)}
                                    key={element._id}
                                    as="li"
                                    // 
                                    style={{cursor:"pointer", background:"#f5f3fe", margin:"6px", padding:"15px", borderRadius:"6px"}}
                                    className="d-flex justify-content-between align-items-start userchatDiv"
                                >
                                    <div className="ms-2 me-auto">
                                        <div className="fw-bold">{element.name}</div>
                                    </div>
                                    <Badge bg="primary" pill>
                                        14
                                    </Badge>
                                </ListGroup.Item>

                            </>)
                        }) : ""
                        }
                    </ListGroup>
                    :
                    <LoadingSkeleton></LoadingSkeleton>
            }
        </div>
    </>)
}

export default SearchedData;