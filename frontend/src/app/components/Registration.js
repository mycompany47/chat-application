"use client";
import React, { useState } from 'react';
import { Container, Button, Col, Form, InputGroup, Row } from "react-bootstrap";
import styles from "../styles/auth.module.css"; // Updated import
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Registration = () => {
  const [validated, setValidated] = useState(false);

  const [data, setData] = useState({
    name: "",
    email: "",
    password: "",
    confirmpassword: "",
    image: "",
  });

  const [err, setErr] = useState({
    nameLabelErr: false,
    emailLabelErr: false,
    passwordLabelErr: false,
    ConfirmpasswordLabelErr: false,
    imageLabelErr: false,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setData((prevData) => ({ ...prevData, [name]: value }));

    if (name === 'name' && value.length > 0) {
      setErr((prevState) => ({ ...prevState, nameLabelErr: false }));
    }

    if (name === 'email' && value.length > 0) {
      setErr((prevState) => ({ ...prevState, emailLabelErr: false }));
    }

    if (name === 'password' && value.length > 0) {
      setErr((prevState) => ({ ...prevState, passwordLabelErr: false }));
    }

    if (name === 'confirmpassword' && value.length === 0) {
      setErr((prevState) => ({ ...prevState, ConfirmpasswordLabelErr: false }));
    }
  };

  const handleChangeImage = (e) => {
    let picFile = e.target.files[0];
    const fileType = picFile.type;
    if (fileType === "image/png" || fileType === "image/jpeg") {
      setData({ ...data, image: picFile });
    } else {
      console.log("please upload only png or jpeg image file");
    }
  };

  const validateData = () => {
    let nameErr, emailErr, passwordErr, imageErr, passwordMismatchErr = false;
    if (data.name.length === 0) {
      nameErr = true;
      setErr((prevState) => ({
        ...prevState,
        nameLabelErr: true,
      }));
    }
    if (data.email.length === 0) {
      emailErr = true;
      setErr((prevState) => ({
        ...prevState,
        emailLabelErr: true,
      }));
    }
    if (data.password.length === 0) {
      passwordErr = true;
      setErr((prevState) => ({
        ...prevState,
        passwordLabelErr: true,
      }));
    }
    if (data.password !== data.confirmpassword) {
      passwordMismatchErr = true;
      setErr((prevState) => ({
        ...prevState,
        ConfirmpasswordLabelErr: true,
      }));
    }
    if (data.image.length === 0) {
      imageErr = true;
      setErr((prevState) => ({
        ...prevState,
        imageLabelErr: true,
      }));
    }
    return !(nameErr || emailErr || passwordErr || imageErr || passwordMismatchErr);
  };

  const submitToRegister = async (event) => {
    event.preventDefault();
    let isValid = validateData();

    if (isValid) {
      let payloadData = new FormData();
      payloadData.append("name", data.name);
      payloadData.append("email", data.email);
      payloadData.append("password", data.password);
      payloadData.append("pic", data.image);

      try {
        const res = await axios.post("http://localhost:3000/api/user/", payloadData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        });
        localStorage.setItem("isloggedChatApp", JSON.stringify(res.data));
        toast("Successfully Registered");
      } catch (error) {
        toast("Something went wrong");
        console.error("Error in API call:", error.response ? error.response.data : error.message);
      }
    }
  };

  return (
    <>
      <div className={styles.MainContainer}>
        <Container>
          <div className={styles.FormBox}>
            <div className={styles.Form}>
              <div className={styles.Heading}>
                <h5>Sign Up</h5>
              </div>
              <Form>
                <Row className="mb-3">
                  <Form.Group as={Col} md="12" controlId="validationCustomUsername">
                    <Form.Label><b>Name</b></Form.Label>
                    <InputGroup hasValidation>
                      <Form.Control
                        type="text"
                        placeholder="name"
                        aria-describedby="inputGroupPrepend"
                        required
                        onChange={handleChange}
                        value={data.name}
                        className={styles.inputBox}
                        name="name"
                      />
                    </InputGroup>
                  </Form.Group>
                  <label className={styles.Errors}>{err.nameLabelErr ? "Please fill the name" : ""}</label>

                  <Form.Group className={styles.FormFeild} as={Col} md="12" controlId="validationCustomUsername">
                    <Form.Label><b>Email</b></Form.Label>
                    <InputGroup hasValidation>
                      <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                      <Form.Control
                        type="email"
                        placeholder="Email"
                        name="email"
                        className={styles.inputBox}
                        aria-describedby="inputGroupPrepend"
                        required
                        onChange={handleChange}
                        value={data.email}
                      />
                    </InputGroup>
                  </Form.Group>
                  <label className={styles.Errors}>{err.emailLabelErr ? "Please fill the email" : ""}</label>

                  <Form.Group as={Col} md="12" className={styles.FormFeild} controlId="validationCustom02">
                    <Form.Label><b>password</b></Form.Label>
                    <Form.Control
                      required
                      type="password"
                      name="password"
                      className={styles.inputBox}
                      value={data.password}
                      placeholder="password"
                      onChange={handleChange}
                    />
                  </Form.Group>
                  <label className={styles.Errors}>{err.passwordLabelErr ? "Please fill the password" : ""}</label>

                  <Form.Group as={Col} md="12" className={styles.FormFeild} controlId="validationCustom02">
                    <Form.Label><b>Confirm password</b></Form.Label>
                    <Form.Control
                      required
                      value={data.confirmpassword}
                      name="confirmpassword"
                      type="password"
                      onChange={handleChange}
                      placeholder="password"
                      className={styles.inputBox}
                    />
                  </Form.Group>
                  <label className={styles.Errors}>{err.ConfirmpasswordLabelErr ? "Password is mismatched" : ""}</label>

                  <Form.Group as={Col} md="12" className={styles.FormFeild} controlId="validationCustomUsername">
                    <Form.Label><b>Upload Your Picture</b></Form.Label>
                    <InputGroup hasValidation>
                      <Form.Control
                        type="file"
                        accept="image/*"
                        name="image"
                        onChange={handleChangeImage}
                        required
                        className={styles.inputBox}
                      />
                    </InputGroup>
                  </Form.Group>
                  <label className={styles.Errors}>{err.imageLabelErr ? "Please upload the image" : ""}</label>
                </Row>

                <div className={styles.BtnBox}>
                  <Button type="submit" className={styles.btnAuth} onClick={submitToRegister}>Register First Time</Button>
                </div>
              </Form>
            </div>
            <ToastContainer />
          </div>
        </Container>
      </div>
    </>
  );
}

export default Registration;
