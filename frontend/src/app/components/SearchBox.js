import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux';
import Offcanvas from 'react-bootstrap/Offcanvas';
import { Row, Col, Form, ListGroup, Badge, Dropdown, Placeholder, Card } from 'react-bootstrap';
import styles from "../styles/chat.module.css"
import ProfileModal from './ProfileModal';
import SearchedData from './SearchedData';
// slice imports
import { allUserThunk, setUser, closeModal } from '@/store/usersSlice';
import ChatBox from './ChatBox';

export const SearchBox = () => {
    const dispatch = useDispatch();
    const [modalShow, setModalShow] = useState(false);
    const [show, setShow] = useState(false);
    const user = useSelector((state) => {
        return state.searchData.user
    })

    const modalClose = useSelector((state) => {
        return state.searchData.showModal
    })

    const handleClose = () => {
        setShow(false);
        dispatch(closeModal(true))

    }

    useEffect(() => {
        if (!modalClose) {
            handleClose()
        }
    }, [modalClose])

    const handleShow = () => setShow(true);

    // handleChange dispatch
    const handleSearch = (e) => {
        dispatch(setUser(e.target.value));
    };

    const submitToSearch = () => {
        console.log("user----", user)
        dispatch(allUserThunk.searchUsers(user));
    }

    return (
        <>

            <div className={styles.parent_status_bar}>
                <div className={styles.status_bar}>
                    <Button variant='' className={styles.GlobalBtn} onClick={handleShow}>
                        Search People
                    </Button>
                    <div className={styles.boxForHeading}>
                        <h5>
                            Talk Application
                        </h5>
                    </div>
                    <div className={styles.boxForNotifications}>
                        <div className={styles.BoxForProfile}>
                            <img style={{ width: "6%" }} src='https://st3.depositphotos.com/15648834/17930/v/450/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg'></img>
                            <Dropdown>
                                <Dropdown.Toggle variant="" id="dropdown-basic">
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1" onClick={() => setModalShow(true)}>User detail</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </div>
                </div>
            </div>

            <Offcanvas show={show} onHide={handleClose}>
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>Search People</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    <div>
                        <Form inline>
                            <div className={styles.RowBox}>
                                <Col xs="auto">
                                    <Form.Control
                                        type="text"
                                        onChange={handleSearch}
                                        placeholder="Search"
                                        className=" mr-sm-2"
                                    />
                                </Col>
                                <Col xs="auto">
                                    <Button onClick={submitToSearch}>Go</Button>
                                </Col>
                            </div>
                        </Form>
                    </div>

                    <SearchedData></SearchedData>

                </Offcanvas.Body>
            </Offcanvas>

            <ChatBox></ChatBox>


            <ProfileModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />

        </>

    )
}
