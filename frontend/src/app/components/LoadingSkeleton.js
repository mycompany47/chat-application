import React from "react";
import { Placeholder , Card } from 'react-bootstrap';

const loadingSkeleton = () => {
    return (<>

        <Placeholder as={Card.Title} animation="glow">
            <Placeholder xs={6} md={12} />
        </Placeholder>
        <Placeholder as={Card.Title} animation="glow">
            <Placeholder xs={6} md={12} />
        </Placeholder>
        <Placeholder as={Card.Title} animation="glow">
            <Placeholder xs={6} md={12} />
        </Placeholder>
        <Placeholder as={Card.Title} animation="glow">
            <Placeholder xs={6} md={12} />
        </Placeholder>
        <Placeholder as={Card.Title} animation="glow">
            <Placeholder xs={6} md={12} />
        </Placeholder>
    </>)
}


export default loadingSkeleton